package far2020;

import java.util.List;

/**
 * A simple model of a rabbit. Rabbits age, move, breed, and die.
 * 
 * @author Anderson Powers
 */
public class Rabbit extends Animal {
	// The age at which a rabbit can start to breed.
	private static final int BREEDING_AGE = 5;

	// The age to which a rabbit can live.
	private static final int MAX_AGE = 40;

	// The likelihood of a rabbit breeding.
	private static final double BREEDING_PROBABILITY = 0.12;

	// The maximum number of births.
	private static final int MAX_LITTER_SIZE = 4;

	/**
	 * Create a new rabbit. A rabbit may be created with age zero (a new born) or
	 * with a random age.
	 * 
	 * @param randomAge If true, the rabbit will have a random age.
	 * @param field     The field currently occupied.
	 * @param location  The location within the field.
	 */
	public Rabbit(boolean randomAge, Field field, Location location) {
		super(randomAge, MAX_AGE, BREEDING_AGE, BREEDING_PROBABILITY, MAX_LITTER_SIZE, field, location);
	}

	/**
	 * This is what the rabbit does most of the time - it runs around. Sometimes it
	 * will breed or die of old age.
	 * 
	 * @param newRabbits A list to return newly born rabbits.
	 */
	public void act(List<Animal> newAnimals) {
		incrementAge();
		if (alive) {

			giveBirth(newAnimals);

			// Try to move into a free location.
			Location newLocation = field.freeAdjacentLocation(location);
			if (newLocation != null) {
				setLocation(newLocation);
			} else {
				// Overcrowding.
				setDead();
			}
		}
	}

	/**
	 * Check whether or not this rabbit is to give birth at this step. New births
	 * will be made into free adjacent locations.
	 * 
	 * @param newRabbits A list to return newly born rabbits.
	 */
	private void giveBirth(List<Animal> newAnimals) {
		// New rabbits are born into adjacent locations.
		// Get a list of adjacent free locations.
		List<Location> free = field.getFreeAdjacentLocations(location);
		int births = breed();
		for (int b = 0; b < births && free.size() > 0; b++) {
			Location loc = free.remove(0);
			Rabbit young = new Rabbit(false, field, loc);
			newAnimals.add(young);
		}
	}
}
