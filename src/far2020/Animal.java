package far2020;

import java.util.List;
import java.util.Random;

/**
 * A class containing the characteristics of animals. Animals can breed, act,
 * age, and die.
 * 
 * @author Anderson Powers
 *
 */
public abstract class Animal {

	protected int age;
	private int maxAge;
	protected int breedingAge;
	protected double breedingProbability;
	protected int maxLitterSize;
	protected boolean alive;
	protected Location location;
	protected Field field;
	protected static final Random rand = Randomizer.getRandom();

	/**
	 * Create a new animal.
	 * 
	 * @param randomAge           If true, the animal will have a random age.
	 * @param maxAge              The age to which an animal can live.
	 * @param breedingAge         The age at which an animal can start to breed.
	 * @param breedingProbability The likelihood of an animal breeding.
	 * @param maxLitterSize       The maximum number of births.
	 * @param field               The field currently occupied by the animal.
	 * @param location            The location within the field.
	 */
	public Animal(boolean randomAge, int maxAge, int breedingAge, double breedingProbability, int maxLitterSize,
			Field field, Location location) {
		this.maxAge = maxAge;
		this.breedingAge = breedingAge;
		this.breedingProbability = breedingProbability;
		this.maxLitterSize = maxLitterSize;
		this.field = field;
		alive = true;
		setLocation(location);
		if (randomAge) {
			age = rand.nextInt(maxAge);
		} else {
			age = 0;
		}
	}

	/**
	 * Check whether the animal is alive or not.
	 * 
	 * @return True if the animal is still alive.
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Return the animal's location.
	 * 
	 * @return The animal's location.
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Increase the age. This could result in the animal's death.
	 */
	protected void incrementAge() {
		age++;
		if (age > maxAge) {
			setDead();
		}
	}

	/**
	 * Indicate that the animal is no longer alive. It is removed from the field.
	 */
	public void setDead() {
		alive = false;
		if (location != null) {
			field.clear(location);
			location = null;
			field = null;
		}
	}

	/**
	 * Place the animal at the new location in the given field.
	 * 
	 * @param newLocation The animal's new location.
	 */
	protected void setLocation(Location newLocation) {
		if (location != null) {
			field.clear(location);
		}
		location = newLocation;
		field.place(this, newLocation);
	}

	/**
	 * Generate a number representing the number of births, if it can breed.
	 * 
	 * @return The number of births (may be zero).
	 */
	protected int breed() {
		int births = 0;
		if (canBreed() && rand.nextDouble() <= breedingProbability) {
			births = rand.nextInt(maxLitterSize) + 1;
		}
		return births;
	}

	/**
	 * An animal can breed if it has reached the breeding age.
	 * 
	 * @return true if the animal can breed, false otherwise.
	 */
	private boolean canBreed() {
		return age >= breedingAge;
	}

	/**
	 * The actions performed by the animal each time the simulation is ran once.
	 * 
	 * @param newAnimals A list to return newly born animals.
	 */
	protected abstract void act(List<Animal> newAnimals);
}
