package far2020;

import java.util.Random;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
// Added the following lines
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A simple predator-prey simulator, based on a rectangular field containing
 * animals
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2016.02.29
 */
public class Simulator {
	// Constants representing configuration information for the simulation.
	// The default width for the grid.
	private static final int DEFAULT_WIDTH = 120;
	// The default depth of the grid.
	private static final int DEFAULT_DEPTH = 80;
	// The probability that a fox will be created in any given grid position.
	private static final double FOX_CREATION_PROBABILITY = 0.02;
	// The probability that a rabbit will be created in any given position.
	private static final double RABBIT_CREATION_PROBABILITY = 0.08;

	// Lists of animals in the field.
	private List<Animal> animals;
	// The current state of the field.
	private Field field;
	// The current step of the simulation.
	private int step;
	// A graphical view of the simulation.
	private SimulatorView view;

	// Added this variable for use by the thread.
	private int numberSteps;

	// Added the following GUI stuff
	// The main window to display the simulation (add your buttons, etc.).
	JButton runOneButton;
	private JFrame mainFrame;

	/**
	 * Construct a simulation field with default size.
	 */
	public Simulator() {
		this(DEFAULT_DEPTH, DEFAULT_WIDTH);
	}

	public static void main(String[] args) {
		// Create the simulator
		Simulator s = new Simulator();
	}

	/**
	 * Create a simulation field with the given size.
	 * 
	 * @param depth Depth of the field. Must be greater than zero.
	 * @param width Width of the field. Must be greater than zero.
	 */
	public Simulator(int depth, int width) {
		if (width <= 0 || depth <= 0) {
			System.out.println("The dimensions must be >= zero.");
			System.out.println("Using default values.");
			depth = DEFAULT_DEPTH;
			width = DEFAULT_WIDTH;
		}

		animals = new ArrayList<>();
		field = new Field(depth, width);

		// Create a view of the state of each location in the field.
		view = new SimulatorView(depth, width);
		view.setColor(Rabbit.class, Color.ORANGE);
		view.setColor(Fox.class, Color.BLUE);

		// The rest of this method has changed. This sets up the
		// JFrame to display everything.

		mainFrame = new JFrame();
		mainFrame.setTitle("Animal Survival Simulation");

		// Add window listener so it closes properly.
		// when the "X" in the upper right corner is clicked.
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Add a button to run 1 steps.
		runOneButton = new JButton("Run 1 step");
		runOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulate(1);
			}
		});
		// Adding things to a JFrame first requires that you get the
		// content pane. Notice you don't do with with a JPanel.
		Container contents = mainFrame.getContentPane();
		contents.add(view, BorderLayout.CENTER);

		// Note: This button was purposely put on the container in
		// an ugly way so that it takes up the entire north panel.
		// It should NOT have been done that way, but if I did it
		// correctly the first time you wouldn't have the opportunity
		// to learn by fixing it.
		//
		// When you add the other buttons improve upon the look
		// of the application. Hint: You might want to use a
		// HorizontalBox or a JPanel with a FlowLayout. Put that on
		// NORTH and put the buttons, etc. on that.
		contents.add(runOneButton, BorderLayout.NORTH);

		// You always need to call pack on a JFrame.
		mainFrame.pack();

		// For very subtle reasons, this MUST go after the pack statement above.
		// Just trust me on this one. Or figure out why for yourself.
		reset();

		// You always need to call setVisible(true) on a JFrame.
		mainFrame.setVisible(true);
	}

	/**
	 * Run the simulation from its current state for a reasonably long period (4000
	 * steps).
	 */
	public void runLongSimulation() {
		simulate(4000);
	}

	/**
	 * Run the simulation for the given number of steps. Stop before the given
	 * number of steps if it ceases to be viable.
	 * 
	 * @param numSteps The number of steps to run for.
	 */
	public void simulate(int numSteps) {
		// For technical reason, I had to add numberSteps as a class variable.
		numberSteps = numSteps;
		// Create a thread
		Thread runThread = new Thread() {
			// When the thread runs, it will simulate numberSteps steps.
			public void run() {
				// Disable the button until the simulation is done.
				runOneButton.setEnabled(false);
				for (int step = 1; step <= numberSteps && view.isViable(field); step++) {
					simulateOneStep();
				}
				// Now re-enable the button
				runOneButton.setEnabled(true);
			}
		};
		// Start the thread
		runThread.start();
		// Now this method exits, allowing the GUI to update. The simulation is being
		// run on a different thread, so the GUI updates as the simulation continues.
	}

	/**
	 * Run the simulation from its current state for a single step. Iterate over the
	 * whole field updating the state of each animal.
	 */
	public void simulateOneStep() {
		step++;

		// Provide space for newborn animals.
		List<Animal> newAnimals = new ArrayList<Animal>();

		// Let all animals act.
		for (Iterator<Animal> it = animals.iterator(); it.hasNext();) {
			Animal animal = it.next();
			animal.act(newAnimals);
			if (!animal.isAlive()) {
				it.remove();
			}
		}

		// Add the newly born animals to the list.
		animals.addAll(newAnimals);

		view.showStatus(step, field);
	}

	/**
	 * Reset the simulation to a starting position.
	 */
	public void reset() {
		step = 0;
		animals.clear();
		populate();

		// Show the starting state in the view.
		view.showStatus(step, field);
	}

	/**
	 * Randomly populate the field with animals.
	 */
	private void populate() {
		Random rand = Randomizer.getRandom();
		field.clear();
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
					Location location = new Location(row, col);
					Fox fox = new Fox(true, field, location);
					animals.add(fox);
				} else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
					Location location = new Location(row, col);
					Rabbit rabbit = new Rabbit(true, field, location);
					animals.add(rabbit);
				}
				// else leave the location empty.
			}
		}
	}

	/**
	 * Pause for a given time.
	 * 
	 * @param millisec The time to pause for, in milliseconds
	 */
	private void delay(int millisec) {
		try {
			Thread.sleep(millisec);
		} catch (InterruptedException ie) {
			// wake up
		}
	}
}
